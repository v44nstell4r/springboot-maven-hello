package com.smaven.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMavenHelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMavenHelloApplication.class, args);
		System.out.println("Help!!");
	}
}
